name = "Partner Emotes"
version = "1.0"
description = ""
author = "Hornet and Goat-Slice"

dst_compatible = true

api_version_dst = 10

--icon_atlas = "modicon.xml"
--icon = "modicon.tex"

all_clients_require_mod = true
client_only_mod = false

folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
	name = name .. " - GitLab Ver."
	description = description .. "\n\nRemember to manually update! The version number does NOT increase with every gitlab update."
end