Assets = {
	Asset("ANIM", "anim/partnerpopup.zip"),
	Asset("ANIM", "anim/taunt_poses.zip"),
}

local env = env
GLOBAL.setfenv(1, GLOBAL)

local AddStategraphPostInit = env.AddStategraphPostInit
local AddPlayerPostInit = env.AddPlayerPostInit
local AddUserCommand = env.AddUserCommand
--

local TALLER_INTERACTOR_OFFSET = Vector3(0, -700, 0)
local function GetInteractorOffset(inst)
    local rider = inst.replica.rider
    return (rider ~= nil and rider:IsRiding() or inst:HasTag("playerghost"))
        and TALLER_INTERACTOR_OFFSET
        or nil
end

AddPlayerPostInit(function(inst)
	inst:AddComponent("partnerinteractor")
    inst.components.partnerinteractor:SetOffsetFn(GetInteractorOffset)
end)

--

PARTNEREMOTES = {
    ["partnertest"] = {
        data = { 
			anim_pre = "taunt_pre",
			anim_idle = "taunt_idle",
			anim_idle_talk = "channel_dial_loop",
			anim_action = "taunt_action",
			--
			icon = "highfive",
		},
    },
}

function c_partnerwilson()
	local wils = c_spawn("wilson")
	
	wils:PushEvent("partneremote", PARTNEREMOTES["partnertest"].data)
end

local function CreateEmoteCommand(emotedef)
    return {
        aliases = emotedef.aliases,
        prettyname = function(command) return string.format(STRINGS.UI.BUILTINCOMMANDS.EMOTES.PRETTYNAMEFMT, FirstToUpper(command.name)) end,
        desc = function() return STRINGS.UI.BUILTINCOMMANDS.EMOTES.DESC end,
        permission = COMMAND_PERMISSION.USER,
        params = {},
        emote = true,
        slash = true,
        usermenu = false,
        servermenu = false,
        vote = false,
        serverfn = function(params, caller)
            local player = UserToPlayer(caller.userid)
            if player ~= nil then
                player:PushEvent("partneremote", emotedef.data)
            end
        end,
        displayname = emotedef.displayname
    }
end

for k, v in pairs(PARTNEREMOTES) do
    AddUserCommand(k, CreateEmoteCommand(v))
end

--[[Stategraph]]--
local states = {
	--(H): wind-up for the partner emote(e.g. raise hand up for high five emote)
	State{
        name = "partneremote_pre",
        tags = { "busy", "pausepredict" },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
			inst.components.partnerinteractor:ActivatePopUp(data.icon)
			inst.components.partnerinteractor.current_partner_data = data
			inst.Transform:SetRotation(RoundBiasedUp(inst.Transform:GetRotation()/90) * 90) --round to 90, 180, 270 or 360 rotation
			
			inst.sg.statemem.emote_data = data

			local anim = data.anim_pre

			inst.AnimState:PlayAnimation(anim, false)

			--[[
            if data.sound then --sound might be a boolean, so don't do ~= nil
                if (data.sounddelay or 0) <= 0 then
                    inst.SoundEmitter:PlaySound(data.sound)
                else
                    inst.sg.statemem.emotesoundtask = inst:DoTaskInTime(data.sounddelay, DoForcedEmoteSound, data.sound)
                end
            elseif data.sound ~= false then
                if (data.sounddelay or 0) <= 0 then
                    DoEmoteSound(inst, data.soundoverride, data.soundlooped)
                else
                    inst.sg.statemem.emotesoundtask = inst:DoTaskInTime(data.sounddelay, DoEmoteSound, data.soundoverride, data.soundlooped)
                end
            end
			]]

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        timeline =
        {
            TimeEvent(.5, function(inst)
                inst.sg:RemoveStateTag("busy")
                inst.sg:RemoveStateTag("pausepredict")
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("partneremote_idle", inst.sg.statemem.emote_data)
                end
            end),
        },

        onexit = function(inst)
            --inst.components.partnerinteractor:CancelPopUp()
        end,
    },
	--(H) the idle for the partner emote while they wait for their partner
	State{
        name = "partneremote_idle",
        tags = {  },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
			
			inst.sg.statemem.emote_data = data
			
			local anim = data.anim_idle

			inst.AnimState:PlayAnimation(anim, false)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        timeline =
        {

        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("partneremote_idle", inst.sg.statemem.emote_data)
                end
            end),
        },

        onexit = function(inst)
            
        end,
    },
	--(H) the actual action for the partner emote (e.g. high five between the two players!)
	State{
        name = "partneremote_action",
        tags = {  },

        onenter = function(inst, data)
            inst.components.locomotor:Stop()
			
			local anim = data.anim_action

			inst.AnimState:PlayAnimation(anim, false)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        timeline =
        {

        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst.components.partnerinteractor.current_partner_data = nil
        end,
    },
}

local events = {
    EventHandler("partneremote", function(inst, data)
		if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (not data.mountonly or inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (data.moose or not inst:HasTag("weremoose"))
            and (data.goose or not inst:HasTag("weregoose")) then
            inst.sg:GoToState("partneremote_pre", data)
        end
    end)
}

AddStategraphPostInit("wilson", function(sg)

	for k, v in pairs(states) do
		sg.states[v.name] = v
	end

	for k, v in pairs(events) do
		sg.events[v.name] = v
	end

end)

--[[Action]]--
local INTERACTWITHPARTNER = Action({})
INTERACTWITHPARTNER.id = "INTERACTWITHPARTNER"
INTERACTWITHPARTNER.str = "This string shouldn't be visible!"

INTERACTWITHPARTNER.fn = function(act)
	if act.target == nil then return end
	if act.doer == nil then return end
	
	local data = act.target.components.partnerinteractor.current_partner_data
	
	act.target.sg:GoToState("partneremote_action", data)
	act.doer.sg:GoToState("partneremote_action", data)
end

env.AddAction(INTERACTWITHPARTNER)

--[[RPC's]]--

AddModRPCHandler("PartnerEmotes", "PushPartnerEmoteAction", function(player, partner)
	if not checkentity(partner) then
		return
	end
	local x, y, z = partner.Transform:GetWorldPosition()
	local rot = -(partner.Transform:GetRotation()) * DEGREES
	local pos = Vector3(x + 2 * math.cos(rot), 0, z + 2 * math.sin(rot))
	local action = BufferedAction(player, nil, ACTIONS.INTERACTWITHPARTNER, nil, pos, nil, 0.1)
	player.components.locomotor:PushAction(action, true)
	action.target = partner --(H): Hack! The game prefers to go to targets position instead of the set pos field, so we set it after PushAction()
	action.options.instant = true
end)

AddClientModRPCHandler("PartnerEmotes", "PopUp", function(target, symbol)
	if target ~= nil and target.components.partnerinteractor ~= nil then
        target.components.partnerinteractor:ActivatePopUp()
    end
end)