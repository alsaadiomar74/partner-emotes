local Widget = require "widgets/widget"
local UIAnimButton = require "widgets/uianimbutton"

local FollowPartnerPopUp = Class(Widget, function(self, symbol)
    Widget._ctor(self, "followpartnerpopup")

    self:SetScaleMode(SCALEMODE_PROPORTIONAL)
    self:SetMaxPropUpscale(MAX_HUD_SCALE)
	
	self.popup = self:AddChild(UIAnimButton("partnerpopup", "partnerpopup"))
	self.popup.animstate:OverrideSymbol("swap_icon", "partnerpopup", symbol)
	self.popup:SetOnClick(function() self:DoPartnerEmote() end)

    self.offset = Vector3(0, 0, 0)
    self.screen_offset = Vector3(0, 0, 0)
	self.buttonenabled = true

    self:StartUpdating()
end)

--(H): Hack! We can't run Disable() because self.enabled gets set to false, and that prevents OnUpdates from running(boo), but we want the button to disable if it's on ourselves so, and uianimbutton calls IsEnabled
--instead of checking the enabled field directly, so. here's this hack!
function FollowPartnerPopUp:IsEnabled()
	return self.buttonenabled
end

function FollowPartnerPopUp:DoPartnerEmote()
    if not ThePlayer:HasTag("busy") then
        self.popup.animstate:PlayAnimation("popdown")
		self.popup.uianim.inst:ListenForEvent("animover", function()
			self:Kill()
		end)
		SendModRPCToServer(GetModRPC("PartnerEmotes", "PushPartnerEmoteAction"), self.target)
    end
end

--This would normally be achieved via an owner argument in the constructor
--but we'll not change the constructor in case MODs are using this widget.
function FollowPartnerPopUp:SetHUD(hud)
    if not self.hashud then
        self.hashud = true
        self.popup:SetScale(TheFrontEnd:GetHUDScale())
        --listen for events (these are not commonly triggered)
        --better than polling update, because GetHUDScale() isn't super cheap
        self.popup.inst:ListenForEvent("continuefrompause", function() self.popup:SetScale(TheFrontEnd:GetHUDScale()) end, hud)
        self.popup.inst:ListenForEvent("refreshhudsize", function(hud, scale) self.popup:SetScale(scale) end, hud)
    end
end

function FollowPartnerPopUp:SetTarget(target)
    self.target = target
    self:OnUpdate()
	self.popup.animstate:PlayAnimation("popup")
	self.popup.animstate:PushAnimation("idle", true)
	
	if self.target == ThePlayer then
		self.buttonenabled = false
	end
end

function FollowPartnerPopUp:SetOffset(offset)
    self.offset = offset
    self:OnUpdate()
end

function FollowPartnerPopUp:SetScreenOffset(x,y)
    self.screen_offset.x = x
    self.screen_offset.y = y
    self:OnUpdate()
end

function FollowPartnerPopUp:GetScreenOffset()
    return self.screen_offset.x, self.screen_offset.y
end

function FollowPartnerPopUp:OnUpdate(dt)
    if self.target ~= nil and self.target:IsValid() then
        if not self.hashud then
            --legacy support for MODs
            self.popup:SetScale(TheFrontEnd:GetHUDScale())
        end
        local x, y
        if self.target.AnimState ~= nil then
            x, y = TheSim:GetScreenPos(self.target.AnimState:GetSymbolPosition(self.symbol or "", self.offset.x, self.offset.y, self.offset.z))
        else
            x, y = TheSim:GetScreenPos(self.target.Transform:GetWorldPosition())
        end
        self:SetPosition(x + self.screen_offset.x, y + self.screen_offset.y, 0)
    end
end

return FollowPartnerPopUp