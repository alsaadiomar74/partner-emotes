local FollowPartnerPopup = require "widgets/followpartnerpopup"

local DEFAULT_OFFSET = Vector3(0, -400, 0)
--
local function CancelPopUp(self)
    if self.widget ~= nil then
        self.widget:Kill()
        self.widget = nil
    end
end

local function PopUpFn(self, symbol, nobroadcast)
	local player = ThePlayer
    if (not self.disablefollowpopup) and self.widget == nil and player ~= nil and player.HUD ~= nil then
        self.widget = player.HUD:AddChild(FollowPartnerPopup(symbol))
        self.widget:SetHUD(player.HUD.inst)
    end
	
	if self.widget ~= nil then
        self.widget.symbol = symbol
        self.widget:SetOffset(self.offset_fn ~= nil and self.offset_fn(self.inst) or self.offset or DEFAULT_OFFSET)
        self.widget:SetTarget(self.inst)
    end
	
	if not nobroadcast then
		SendModRPCToClient(GetClientModRPC("PartnerEmotes", "PopUp"))
	end
end
--

local PartnerInteractor = Class(function(self, inst)
    self.inst = inst
	self.disablefollowpopup = nil
end)

function PartnerInteractor:ActivatePopUp(symbol, force)
    if TheWorld.ismastersim then
        if not force
            and (self.ignoring ~= nil or
                (self.inst.components.health ~= nil and self.inst.components.health:IsDead() and self.inst.components.revivablecorpse == nil) or
                (self.inst.components.sleeper ~= nil and self.inst.components.sleeper:IsAsleep())) then
            return
        elseif self.ontalk ~= nil then
            self.ontalk(self.inst, script)
        end
    elseif not force then
        if self.inst.components.revivablecorpse == nil then
			if IsEntityDead(self.inst) then
                return
            end
        end
    end
	
	CancelPopUp(self)
	PopUpFn(self, symbol)
end

function PartnerInteractor:CancelPopUp()
	CancelPopUp(self)
end

function PartnerInteractor:SetOffsetFn(fn)
    self.offset_fn = fn
end

return PartnerInteractor